#!/bin/bash
#this is the central scan process
#new scan is startet via scanimage which produces a tiff file
#tiff is convertet to pdf via tiff2pdf
#if several pages are scanned in the given interval (see move_complete.sh) the pages are merged
set -x

cd /home/pi/Scanner;

##these ar my settings with a Canon Scan Lide 120. It may be little bright so play a little with contrast and brightness
scanimage -x 212 -y 299 -p --resolution 300dpi --format=tiff --contrast=38 --brightness=45 --mode Color >scan.tiff
#convert output to pdf
tiff2pdf -j -q 85 -o scan.pdf scan.tiff

#if there already exists a scan from before, append (pdfunite) the new scan to that one
if test -f "target.pdf"
then
    mv target.pdf old.pdf
	sleep 1
	pdfunite old.pdf scan.pdf target.pdf
else
	mv scan.pdf target.pdf
fi

set +x
