# RaspiScanner
I had all of the hardware laying around and wanted a more comfortable way to scan my letters than booting my PC up and connecting my flatbed scanner (Canon Lide 120) and then start the scanning software every time. (Also I don't want to spend several 100 Euros on a better scanner.. so far).
The goal was to scan a complete letter with several pages into one pdf, run OCR and then transfer the document to a remote share (for example to another Linux server via SSH/SCP or to your Nextcloud instance via CURL).

## Hardware
For this project I use following hardware:
- Raspberry Pi + SD (because of pretty large USB current of the scanner at least a Pi 2 if your scanner has dedicated power supply a Pi 1/0 should suffice. I use a Pi 3 because of in-built WiFi)
- Pi Sense Hat for easy input to start scan and a little user output via LED matrix
  - I tried "scanbd" to use the buttons of the scanner directly but unfortunately did not get it to work (write me if you do!)
- an old USB-Scanner (Canon Lide 120)
- a potent 5V power supply (2A)

## Prerequisites
this is probably not yet complete
- Raspian installed
- all necessary programms installed (via apt), for example scanimage, tiff2pdf, ocrmypdf (for this see Pi manual below), maybe others...
- set max_usb_current=1 in /boot/config.txt for a sufficient power supply of the scanner
- ...sorry this thing runs for a little while now and I don't remember if I have done other stuff. But the most important steps should be named.

## Usage
I used following folder for my scanner-project:
/home/pi/Scanner
It is referred to several times in the source code, so either also use this directory or replace it with another one if you like

Look into the source file comments on how to use them. Essentially "start_watcher.sh" has to be run once (e.g. by cron) and then the rest will react on input, so look here for start (the project should be small enough to get the idea).

## Howto install ocrmypdf on a Pi:

Copied with many thanks from:
https://www.heise.de/forum/c-t/Kommentare-zu-c-t-Artikeln/Durchsuchbare-PDF-Dokumente-mit-OCRmyPDF/Schwierigkeiten-bei-der-Einrichtung-von-OCRmy-PDF-unter-K-ubuntu-18-04-LTS/thread-6207926/page-2/#posting_35847270

this procedure will take a while(!) because of compiling code
```
sudo apt update
sudo apt dist-upgrade 
sudo apt install ghostscript libxml2 tesseract-ocr tesseract-ocr-eng tesseract-ocr-deu pngquant unpaper
sudo apt install leptonica-progs libleptonica-dev automake libtool zlib1g-dev libjpeg-dev
sudo apt install python3 python3-pip libxml2-dev libxslt1-dev libffi-dev # evtl. schon da
git clone https://github.com/agl/jbig2enc
cd jbig2enc
./autogen.sh
./configure && make
sudo make install
cd .. 
git clone https://github.com/qpdf/qpdf 
cd qpdf
./configure && make
sudo make install
sudo ldconfig
cd ..
sudo pip3 install --upgrade pip
sudo pip3 install pybind11
sudo pip3 install ocrmypdf
```
