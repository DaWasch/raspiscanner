#!/bin/bash
#use this script for finishing pdf workflow immediately
#it will be called by move_complete.sh if target.pdf is old enough
set -x

#create unique name for document
PDF=$(date +"%Y%m%d%H%M%S").pdf

cd /home/pi/Scanner;

#if target.pdf exists
if test -f "target.pdf"
then
	cp target.pdf $PDF;
	mv target.pdf target_old.pdf;
	#add ocr information replace "deu" with "eng" for English
	python3 ocrpdf.py $PDF $PDF deu;
	#copy complete pdf to target folder(s)
	#scp (ssh) example:
	scp $PDF <user>@<ip-adress>:/target/folder/on/remote/host;
	#nextcloud curl example 
	curl -k -T $PDF -u "<nextcloud shared folder id>:<password of folder>" -H 'X-Requested-With: XMLHttpRequest' https://<nextcloud-url>/public.php/webdav/$PDF;
	#backup document locally to prevent data loss
	mv $PDF out/$PDF;
fi
set +x