#!/bin/bash
#this script has to be run as cron job every 2 minutes or so 
#it checks if an target.pdf file exists that was not touched for 5 minutes
#this puffer time should be enough that a scan with serveral pages should be done
#remind that you also have to wait up to 5+2 minutes if you want to start a new document
#if you want decerase the 5 minutes

set -x

#create unique name for document
PDF=$(date +"%Y%m%d%H%M%S").pdf
#absoulte path necessary here (or parameter through cron)
cd /home/pi/Scanner/;

#if target.pdf was not touched for 5 minutes
if test -f "target.pdf" && test `find "target.pdf" -mmin +5`
then
	./move_now.sh
fi
set +x
