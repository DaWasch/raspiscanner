import subprocess
import time
from sense_hat import SenseHat
from multiprocessing import Process
sense = SenseHat()

def scan():
  #you have to use absolut paths here
  subprocess.call(['/home/pi/Scanner/scan.sh']) 

def showMessage():
  sense.show_message("Start Scan")
  time.sleep(25)
  sense.show_message("End Scan")

if __name__ == '__main__':
    sense.show_message("Welcome!")
    while True:
        event = sense.stick.wait_for_event()
        print(event.direction, event.action)
        if event.action == 'pressed':
              p1 = Process(target=scan)
              p1.start()
              p2 = Process(target=showMessage)
              p2.start()
              p1.join()
              p2.join()
